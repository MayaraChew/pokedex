import styled from "styled-components";
import {
  Box,
  ImgLogo,
  Title,
  DivCentral,
  ImgPerson,
  InputName,
  Button
} from "../components/login_box.js";

export const List = styled.ul`
  font-size: 100%;
  text-align: center;
  list-style-type: none;
  margin-top: 10%;
`;

export const Item = styled.li`
  background-color: #E4F5FA;
  border-radius: 10%;
  width: 100%;
  height: 100%;
  margin: 2%;
  margin-left: -1vw;

  @media (max-width: 600px) {
    margin-left: -4vw;
  }
`;

export const Bold = styled.span`
  font-weight: bold;
`;

export const ImagePoke = styled.img`
  text-align: center;
  width: 50%;
  height: 50%;
  margin: -8%;
`;

export const TitlePoke = styled(Title)`
  font-size: 2.5em;
`;

