import styled from "styled-components";

export const Box = styled.div`
  height: 50vh;
  width: 35vw;
  text-align: center;
  overflow: hidden;
  background-color: white;
  box-shadow: inset 0 0 1em #cce6ff;
  padding: 1%;
  border-radius: 5%;

  @media (max-width: 900px) {
    width: 60vw;
  }

  @media (max-height: 900px) {
    height: 60vh;
  }

  @media (max-width: 450px) {
    width: 70vw;
  }
`;

export const ImgLogo = styled.img`
  width: 10vh;
  height: auto;
  padding-top: 7vh;

  @media (max-width: 450px) {
    padding-top: 7.5vh;
    padding-bottom: 2.5vh;
  }
`;

export const Title = styled.h1`
  @import url('https://fonts.googleapis.com/css?family=Merriweather&display=swap');
  color: #2f414d;
  font-size: 5vh;
  word-wrap: break-word;
  font-family: 'Merriweather', serif;

  @media (max-height: 650px) {
    font-size: 7vh;
  }
`;

export const DivCentral = styled.div`
  max-width: 100%;
  max-height: 100%;
  display: flex;
  align-self: center;
  align-items: center;
  justify-content: center;
`;

export const ImgPerson = styled.img`
  width: 4vw;
  height: 4vw;
  margin-right: 1vw;

  @media (max-width: 650px) {
    width: 8vw;
    height: 8vw;
  }

  @media (max-height: 650px) {
    width: 8vh;
    height: 8vh;
  }
`;

export const InputName = styled.input`
  width: 20vw;
  height: 2vh;
  border-radius: 5%;
  padding: 2%;
  text-align: center;

  @media (max-width: 650px) {
    width: 30vw;
  }
`;

export const Button = styled.button`
  padding: 1vh 1vw 1vh 1vw;
  border-radius: 20%;
  background-color: #E4F5FA;
  border-color: #E4F5FA;
  color: #2f414d;
  text-decoration: none;
  margin: 0.5vw;
  width: 15vw;
  height: 4vh;

  &:hover {
    background-color: #B0E7F7;
    border-color: #B0E7F7;
    transition-duration: 0.5s;
  }

  @media (max-width: 650px) {
    width: 15vw;
  }

  @media (max-height: 650px) {
    height: 6vh;
  }

  @media (max-width: 450px) {
    width: 20vw;
  }

  @media (max-width: 350px) {
    width: 25vw;
  }
`;
