import styled from "styled-components";
import { Title, Button } from "../components/login_box.js";
import imgFundo from "../images/fundo.jpeg";

export const CenterItems = styled.div`
  width: 100%;
  height: 100%;
  text-align: center;
  /* box-shadow: inset 0 0 2em #b0c4de; dando problema*/
  padding: 1%;
`;

export const ColumnsPoke = styled.div`
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  justify-content: space-evenly;
  align-content: space-evenly;
  align-items: center;
  width: 100%;
  height: 100%;
  margin-top: 4vh;

  @media (max-width: 1400px) {
    grid-template-columns: repeat(5, 1fr);
  }

  @media (max-width: 1250px) {
    grid-template-columns: repeat(4, 1fr);
  }

  @media (max-width: 950px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (max-width: 750px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: 450px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;

export const RowsPoke = styled.button`
  height: 25vh;
  width: 25vh;
  grid-template-rows: repeat(25, 1fr);
  overflow: hidden;
  text-align: center;
  justify-self: center;
  align-self: center;
  border-radius: 10%;
  background-color: #B0E7F7;
  padding-bottom: 6vh;
  margin-bottom: 4vh;
  box-shadow: 0 1em 1em lightgray;

  &:hover {
    box-shadow: inset 0 0 3em white;
    transition-duration: 0.5s;
  }

  @media (max-height: 800px) {
    height: 30vh;
    width: 30vh;
  }

  @media (max-height: 650px) {
    height: 35vh;
    width: 35vh;
  }

  @media (max-height: 550px) {
    height: 40vh;
    width: 40vh;
  }

  @media (max-height: 450px) {
    height: 45vh;
    width: 45vh;
  }

  @media (max-height: 300px) {
    height: 50vh;
    width: 50vh;
  }
`;

export const NamePoke = styled.div`
  font-family: "Alegreya", serif;
  font-size: 180%;
`;

export const ImagePoke = styled.img`
  width: 95%;
  height: 95%;
`;

export const TitleInicial = styled(Title)`
  font-size: 3.5em;
  margin-top: -2vh;
  padding: 3%;
  background-image: url(${imgFundo});
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0 0 0.3em lightgray;
`;

export const ButtonLoginInicial = styled(Button)`
  margin-top: -1vh;
  border-radius: 20%;
  text-align: center;

  @media (max-width: 760px) {
    width: 20vw;
    height: 5vh;
  }
`;
