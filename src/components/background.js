import styled from "styled-components";
import imgFundo from '../images/fundo.jpeg'; 

export const Container = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  background-image: url(${imgFundo});
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
`;