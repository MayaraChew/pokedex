import styled from "styled-components";
import imgFundo from "../images/fundo.jpeg";

export const Box = styled.div`
  width: 50%;
  height: 75%;
  text-align: center;
  overflow: hidden;
  background-color: white;
  box-shadow: inset 0 0 1em #cce6ff;
  padding: 2%;
  border-radius: 2%;
`;

export const DivCentral = styled.div`
  text-align: center;
`;

export const Adapt = styled.div`
  font-size: 2vh;
`;

export const Title = styled.h1`
  @import url("https://fonts.googleapis.com/css?family=PT+Serif&display=swap");
  font-family: "PT Serif", serif;
  color: #2f414d;
  font-size: 4vh;
  margin: -0.001vh;
  margin-top: -2vh;
  padding: 3%;
  background-image: url(${imgFundo});
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0 0 0.3em lightgray;
`;

export const Line = styled.hr`
  width: 40vw;
  border-top: 0.1vh solid #c7e6fd;
`;

export const ImgPerfil = styled.img`
  padding-top: 2vh;
  width: 15vh;
  height: auto;
`;

export const ColumnsPokeFav = styled.div`
  margin: -1%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  margin-left: 1vw;
`;

export const ButtonPokesFav = styled.button`
  width: 8vh;
  height: 8vh;
  border-right-color: #B0E7F7;
  background-color: #B0E7F7;
  margin-bottom: 5%;
  margin-right: 1%;
  &:hover {
    box-shadow: inset 0 0 3em white;
    transition-duration: 0.5s;
  }
`;

export const ImgPokeFav = styled.img`
  width: 100%;
  height: 100%;
`;

export const InputPokename = styled.input`
  padding: 1%;
  text-align: center;
`;

export const AddButton = styled.button`
  background-color: #98fb98;
  margin-bottom: 2%;
  text-align: center;
  border-radius: 20%;

  &:hover {
    box-shadow: inset 0 0 3em #c0e9d5;
    transition-duration: 0.5s;
  }
`;

export const DelButton = styled.button`
  background-color: #fa8072;
  margin-bottom: 2%;
  text-align: center;
  border-radius: 20%;

  &:hover {
    box-shadow: inset 0 0 3em #FFA07A;
    transition-duration: 0.5s;
  }
`;

export const TitleName = styled.h1`
  @import url("https://fonts.googleapis.com/css?family=PT+Serif&display=swap");
  font-family: "PT Serif", serif;
  color: #2f414d;
  font-size: 4vh;
`;
