import axios from "axios";

const url = axios.create({ baseURL: "https://pokedex-cjr.herokuapp.com/" });

// Retorna todos os Pokemons
export const GetAllPokemons = N => {
  return url.get(`/pokemons/?page=${N}`);
};

// Retorna os dados do Pokemon de nome <poke>
export const GetPokemon = poke => {
  return url.get(`/pokemons/${poke}`, { poke });
};

// Cria um usuário de nome <user>
export const CreateUser = user => {
  return url.post("/users/", { username: user });
};

//  Retorna dados do usuário de nome <user>
export const GetUser = user => {
  return url.get(`/users/${user}`);
};

// Adiciona um Pokemon de nome <poke> ao usuário de nome <user>
export const AddPokemon = (user, poke) => {
  return url.post(`/users/${user}/starred/${poke}`, { user, poke });
};

// Deleta um Pokemon de nome <poke> do usuário de nome <user>
export const DelPokemon = (user, poke) => {
  return url.delete(`/users/${user}/starred/${poke}`, { user, poke });
};
