import React from "react";
import { GetPokemon } from "../api.js";
import {
  ColumnsPoke,
  RowsPoke,
  NamePoke,
  TitleInicial,
  ButtonLoginInicial
} from "../components/pokemons_edit.js";
import { Container } from "../components/background.js";
import {
  Box,
  ImgLogo,
  Title,
  DivCentral,
  ImgPerson,
  InputName,
  Button
} from "../components/login_box.js";
import {
  List,
  Item,
  Bold,
  ImagePoke,
  TitlePoke
} from "../components/pokemon_details.js";

export default class Pokemon extends React.Component {
  state = {
    pokemon: []
  };

  componentDidMount() {
    GetPokemon(JSON.parse(localStorage.getItem("pokename")))
      .then(res => {
        this.setState({
          pokemon: res.data
        });
        console.log(res.data);
      })
      .catch(error => {
        alert("erro");
      });
  }

  render() {
    return (
      <>
        <Container>
          <Box>
            <TitlePoke>
              #{this.state.pokemon.id} {this.state.pokemon.name}
            </TitlePoke>
            <ImagePoke src={this.state.pokemon.image_url}></ImagePoke>
            <List>
              <Item>
                <Bold>Altura:</Bold> {this.state.pokemon.height} m
              </Item>
              <Item>
                <Bold>Peso:</Bold> {this.state.pokemon.weight} kg
              </Item>
              <Item>
                <Bold>Tipo:</Bold> {this.state.pokemon.kind}
              </Item>
            </List>
          </Box>
        </Container>
      </>
    );
  }
}
