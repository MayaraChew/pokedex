import React from "react";
import { navigate } from "@reach/router";
import { GetAllPokemons, GetPokemon } from "../api.js";
import { CenterItems } from "../components/pokemons_edit.js";
import {
  ColumnsPoke,
  RowsPoke,
  NamePoke,
  ImagePoke,
  TitleInicial,
  ButtonLoginInicial
} from "../components/pokemons_edit.js";
import { Button } from "../components/login_box.js";

export default class Perfil extends React.Component {
  state = {
    pokemons: [],
    page: "number"
  };

  componentDidMount() {
    GetAllPokemons(1)
      .then(res => {
        this.setState({
          pokemons: res.data.data
        });
        this.setState({
          page: res.data.next_page
        });
        console.log(res.data);
      })
      .catch(error => {
        alert("Não foi possível carregar todos os Pokemons.");
      });
  }

  morePokemons = event => {
    event.preventDefault();
    GetAllPokemons(this.state.page)
      .then(res => {
        this.setState({
          pokemons: res.data.data
        });
        this.setState({
          page: res.data.next_page
        });
        console.log(res.data.data);
      })
      .catch(error => {
        alert("Não foi possível carregar mais Pokemons.");
       })
      }

  login = event => {
    event.preventDefault();
    navigate(`/login/`);
  };

  details = pokemon => {
    GetPokemon(pokemon.name)
      .then(res => {
        console.log(pokemon);
        localStorage.setItem("pokename", JSON.stringify(pokemon.name));
        navigate(`/pokemon/`);
      })
      .catch(error => {
        alert("Não foi possível visualizar o Pokemon");
      });
  };

  render() {
    return (
      <>
        <CenterItems>
          <TitleInicial>Pokedex</TitleInicial>
          <ButtonLoginInicial onClick={this.login}>Login</ButtonLoginInicial>
          <ButtonLoginInicial onClick={this.morePokemons}>Mais Pokemons</ButtonLoginInicial>
          <ColumnsPoke>
            {this.state.pokemons.map(pokemon => {
              return (
                <RowsPoke onClick={e => this.details(pokemon)}>
                  <ImagePoke src={pokemon.image_url}></ImagePoke>
                  <NamePoke>{pokemon.name}</NamePoke>
                </RowsPoke>
              );
            })}
          </ColumnsPoke>
        </CenterItems>
      </>
    );
  }
}
