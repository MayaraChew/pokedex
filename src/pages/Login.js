import React from "react";
import imgPokebola from "../images/pokebola.jpeg";
import imgPerson from "../images/pessoa.jpeg";
import { Container } from "../components/background.js";
import {
  Box,
  ImgLogo,
  Title,
  DivCentral,
  ImgPerson,
  InputName,
  Button
} from "../components/login_box.js";
import { navigate } from "@reach/router";
import { GetUser, CreateUser } from "../api.js";

export default class Login extends React.Component {
  state = {
    name: ``
  };

  updateName = event => {
    this.setState({ name: event.target.value });
  };

  handleBack = event => {
    navigate(`/`);
  };

  createPerfil = event => {
    event.preventDefault();
    CreateUser(this.state.name)
      .then(res => {
        localStorage.setItem("username", JSON.stringify(this.state.name));
        // JSON.stringify(res.data.user.username)
        alert("Usuário cadastrado com sucesso.");
      })
      .catch(error => {
        alert("Não foi possível cadastrar o usuário, usuário existente.");
      });
  };

  loginPerfil = event => {
    event.preventDefault();
    GetUser(this.state.name)
      .then(res => {
        localStorage.setItem(
          "username",
          JSON.stringify(res.data.user.username)
        );
        navigate(`/perfil/`);
      })
      .catch(error => {
        alert("Usuário não cadastrado.");
      });
  };

  goBack = event => {
    event.preventDefault();
    navigate(`/`);
  };

  render() {
    return (
      <>
        <Container>
          <Box>
            {/* {this.state.error ? <Erro/> : undefined} control /*/}
            <ImgLogo src={imgPokebola} />
            <Title>Pokedex</Title>
            <DivCentral>
              <ImgPerson src={imgPerson} />
              <InputName
                value={this.state.name}
                onChange={this.updateName}
                type="text"
                placeholder="Nickname"
              ></InputName>
            </DivCentral>
            <br></br> <br></br>
            <DivCentral>
              <Button onClick={this.handleBack}>Voltar</Button>
              <Button onClick={this.createPerfil}>Cadastrar</Button>
              <Button onClick={this.loginPerfil}>Entrar</Button>
            </DivCentral>
          </Box>
        </Container>
      </>
    );
  }
}
