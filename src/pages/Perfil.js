import React from "react";
import { AddPokemon, DelPokemon, GetPokemon } from "../api.js";
import { Container } from "../components/background.js";
import {
  Box,
  Title,
  Line,
  ImgPerfil,
  ButtonPokesFav,
  Adapt,
  ColumnsPokeFav,
  ImgPokeFav,
  DivCentral,
  InputPokename,
  AddButton,
  DelButton,
  TitleName
} from "../components/perfil_back.js";
import imgPerson from "../images/perfil.png";
import { GetUser } from "../api.js";
import { navigate } from "@reach/router";
import { Button } from "../components/login_box.js";
import {
  ColumnsPoke,
  RowsPoke,
  NamePoke,
  ImagePoke,
  TitleInicial,
  ButtonLoginInicial
} from "../components/pokemons_edit.js";

export default class Perfil extends React.Component {
  state = {
    username: ``,
    pokename: ``,
    pokemons: []
  };

  componentDidMount() {
    //requisição antes de recarregar a página

    this.setState({
      username: JSON.parse(localStorage.getItem("username"))
    });

    GetUser(JSON.parse(localStorage.getItem("username")))
      //se substituir por username, da erro pois não deu tempo de atualizar a variável

      .then(res => {
        this.setState({ pokemons: res.data.pokemons });
        console.log(res.data.pokemons);
      })
      .catch(error => {
        alert("erro");
      });
  }

  details = pokemon => {
    GetPokemon(pokemon.name)
      .then(res => {
        console.log(pokemon);
        localStorage.setItem("pokename", JSON.stringify(pokemon.name));
        navigate(`/pokemon/`);
      })
      .catch(error => {
        alert("erro");
      });
  };
  
  updatePokename = event => {
    event.preventDefault();
    this.setState({ pokename: event.target.value });
  };

  addNewPoke = event => {
    event.preventDefault();
    console.log(this.state.pokename);
    console.log(this.state.username);
    AddPokemon(this.state.username, this.state.pokename)
      .then(res => {
        navigate(`/perfil`);
        alert("Novo Pokemon favorito.");
      })
      .catch(error => {
        alert("Não foi possível adicionar esse novo Pokemon aos favoritos.");
      });
  };

  delPoke = event => {
    event.preventDefault();
    console.log(this.state.pokename);
    console.log(this.state.username);
    DelPokemon(this.state.username, this.state.pokename)
      .then(res => {
        navigate(`/perfil`);
        alert("Pokemon deletado com sucesso, recarregue a página.");
      })
      .catch(error => {
        alert("Não foi possível deletar este Pokemon dos favoritos.");
      });
  };

  goBack = event => {
    event.preventDefault();
    navigate(`/login`);
  };

  render() {
    return (
      <>
          <DivCentral>
            <br></br>
            <Title>Perfil de treinador</Title>
            <br></br>
            <ImgPerfil src={imgPerson}></ImgPerfil>
            <TitleName>{this.state.username}</TitleName>
            <Adapt>
              <Line></Line>
              <p>
                <strong>Status:</strong> Mestre pokemon
              </p>
              <Line></Line>
              <p>
                <strong>Localização:</strong> Pokecity
              </p>
              <Line></Line>
              <p>
                <strong>Pokemons favoritos:</strong>
              </p>
              <br></br>
              <ColumnsPokeFav>
                {this.state.pokemons.map(pokemon => {
                  console.log(pokemon.name);
                  return (
                    <ButtonPokesFav onClick={e => this.details(pokemon)}>
                      <ImgPokeFav src={pokemon.image_url}></ImgPokeFav>
                    </ButtonPokesFav>
                  );
                })}
              </ColumnsPokeFav>
              <InputPokename
                value={this.state.pokename}
                onChange={this.updatePokename}
                type="text"
                placeholder="Pokename"
              ></InputPokename>
              <div>
                <AddButton onClick={this.addNewPoke}>ADICIONAR</AddButton>
                <DelButton onClick={this.delPoke}>DELETAR</DelButton>
              </div>
              <br></br>
              <Button onClick={this.goBack}>Sair</Button>
            </Adapt>
          </DivCentral>
      </>
    );
  }
}
