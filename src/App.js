import React from "react";
import { Router } from "@reach/router";
import Inicial from "./pages/Inicial";
import Login from "./pages/Login";
import Perfil from "./pages/Perfil";
import Pokemon from "./pages/Pokemon";

//import './index.css';

const NotFound = () => <h1>404 - Not Found :( </h1>;

export default class Route extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Inicial path="/" />
          <Login path="/login/" />
          <Perfil path="/perfil/" />
          <Pokemon path="/pokemon/" />
          <NotFound default />
        </Router>
      </>
    );
  }
}
